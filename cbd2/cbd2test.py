"""
 Copyright (c) 2021 CSAN_LiU

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 """

import sys
import warnings
warnings.simplefilter("ignore", UserWarning)
sys.coinit_flags = 2
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import keyboard
import os
import random
import time
from time import strftime
from datetime import datetime
import win32gui


# !! Run as administrator !"

# path to params file
PARAMS_FOLDER = "Parameters"
PARAMS_FILE = "params.txt"
WORDS_FILE = "words.txt"
LOG_FOLDER = "_LOGS"
MAX_WORDS_IN_A_ROW = 6

# categories of words
CATEGORY_DICT = {"Gropigt":"Sensory",
                "Brännande":"Sensory",
                "Kallt":"Sensory",
                "Fuktigt":"Sensory",
                "Torrt":"Sensory",
                "Fast":"Sensory",
                "Fluffigt":"Sensory",
                "Luddigt":"Sensory",
                "Fet":"Sensory",
                "Grynigt":"Sensory",
                "Hårigt":"Sensory",
                "Hårt":"Sensory",
                "Hett":"Sensory",
                "Kantig":"Sensory",
                "Knöligt":"Sensory",
                "Stickigt":"Sensory",
                "Skrovligt":"Sensory",
                "Gummiaktigt":"Sensory",
                "Vasst":"Sensory",
                "Halt":"Sensory",
                "Lent":"Sensory",
                "Mjukt":"Sensory",
                "Klibbigt":"Sensory",
                "Darrigt":"Sensory",
                "Varmt":"Sensory",
                "Vått":"Sensory",
                "Upphetsande":"Emotional",
                "Lugnande":"Emotional",
                "Bekvämt":"Emotional",
                "Begärligt":"Emotional",
                "Obekvämt":"Emotional",
                "Trevligt":"Emotional",
                "Spännande":"Emotional",
                "Irriterande":"Emotional",
                "Njutbart":"Emotional",
                "Avslappnande":"Emotional",
                "Sensuellt":"Emotional",
                "Sexigt":"Emotional",
                "Lindrande":"Emotional",
                "Nervkittlande":"Emotional"
                }

###############################################################################
# MY APP CLASSES
# App icon (don't change this here!)
# ICO = ':/icons/app_icon'
########################################
# SETTINGS CLASS
######################################
class MySettingsWidget(QMainWindow):

    valid_input_sig = pyqtSignal()
    def __init__(self):
        super(MySettingsWidget, self).__init__()
        self.name = "Session Settings"
        self.app_width = 400
        self.app_height = 80
        self.setWindowTitle(self.name)
        # self.setWindowIcon(QtGui.QIcon(ICO))
        self.resize(self.app_width,self.app_height)

        self.task_params_dict = {"sessionID":"",
                                    "participantID":"",
                                    "sides":[],
                                    "spots":{},
                                    "repetitions":1,
                                    "sleep":0.5,
                                    "words":[]
                                }

        # read params from file
        self.read_params()

        self.main_widget = QWidget()
        self.main_layout = QFormLayout()
        self.setCentralWidget(self.main_widget)       
        self.main_widget.setLayout(self.main_layout)
        self.participant_id_label = QLabel("Participant ID:")
        self.participant_id_text = QLineEdit(self.task_params_dict["participantID"])
        self.main_layout.addRow(self.participant_id_label,self.participant_id_text)
        self.session_id_label = QLabel("Session ID:")
        self.session_id_text = QLineEdit(self.task_params_dict["sessionID"])
        self.main_layout.addRow(self.session_id_label,self.session_id_text)

        self.start_btn = QPushButton("Start")
        self.main_layout.addRow("",self.start_btn)

        self.start_btn.clicked.connect(self.read_user_input)
        self.valid_input_sig.connect(self.start_task)

    # define keypress events
    def keyPressEvent(self,event):
        # if enter is pressed start button clicked
        if event.key() == Qt.Key_Return:
            self.read_user_input()

    def read_params(self):
        params_folder = os.path.join(os.path.abspath(os.getcwd()),PARAMS_FOLDER)
        params_path = os.path.join(params_folder,PARAMS_FILE)
        spots = {}
        # read txt file with params
        with open(params_path) as f:
            for line in f:
                line_head = line.strip().split(":")
                if line_head[0] == "sessionID":
                    self.task_params_dict["sessionID"] = line_head[1]
                if line_head[0] == "participantID":
                    self.task_params_dict["participantID"] = line_head[1]
                if line_head[0] == "repetitions":
                    try:
                        self.task_params_dict["repetitions"] = int(line_head[1])
                    except:
                        self.show_info_dialog("Number of repetitions has to be an integer.")
                if line_head[0] == "sleepTime":
                    try:
                        self.task_params_dict["sleep"] = float(line_head[1])
                    except:
                        self.show_info_dialog("SleepTime has to be an integer.")
                if line_head[0] == "side":
                    sides = line_head[1].split(",")
                    self.task_params_dict["sides"] = sides
                if line_head[0].startswith("spot"):
                    # get last character to check which side it belongs to
                    last_character = list(line_head[0])[-1]
                    # get spot id as string
                    temp_spots = line_head[1].split(",")
                    spots[last_character] = temp_spots
        self.task_params_dict["spots"] = spots
        # read txt file with words (one word in each line)
        words_path = os.path.join(params_folder,WORDS_FILE)
        words = []
        with open(words_path,encoding="utf-8") as f:
            for line in f:
                words.append(line.strip())
        self.task_params_dict["words"] = words
        print(self.task_params_dict)

    def read_user_input(self):
        
        self.participant_id = self.participant_id_text.text()
        self.session_id = self.session_id_text.text()
        if len(self.participant_id) > 0 and len(self.session_id) > 0:
            self.valid_input_sig.emit()
        else:
            print("Enter participant and session id")
            self.show_info_dialog("Please enter participant and session id!")

    @pyqtSlot()
    def start_task(self):
        self.close()
        self.task_presentation = ManagerWidget([self.task_params_dict])
        self.task_presentation.showMinimized()

     # show info that only ins are streamed
    def show_info_dialog(self, text):
        msgBox = QMessageBox()
        msgBox.setText(text)
        msgBox.setWindowTitle("Info!")
        # msgBox.setWindowIcon(QtGui.QIcon(ICO))
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.exec()

########################## end MySettingsWidget

########################################
# MANAGER CLASS
######################################

class ManagerWidget(QMainWindow):

    def __init__(self,params):
        super(ManagerWidget, self).__init__()
        self.name = "Manager"
        self.app_width = 800
        self.app_height = 600
        self.setWindowTitle(self.name)
        # self.setWindowIcon(QtGui.QIcon(ICO))
        self.resize(self.app_width,self.app_height)
        # set background to black
        self.setStyleSheet("background-color: rgb(0,0,0);") 
        self.current_trial = 0
        self.spot_counter = 0

        self.scale_window = None
        self.words_window = None

        # save parameters to class variables
        self.participant_id = params[0]["participantID"]
        self.session_id = params[0]["sessionID"]
        self.repetitionsNo = params[0]["repetitions"]
        self.sides = params[0]["sides"]
        self.spots_dict = params[0]["spots"]
        self.words = params[0]["words"]
        self.sleep = params[0]["sleep"]


        self.main_widget = QWidget()
        self.setCentralWidget(self.main_widget)
        self.main_layout = QGridLayout()
        self.main_layout.setContentsMargins(10,10,10,10)
        self.main_layout.setAlignment(Qt.AlignCenter)
        self.end_label = QLabel("The End")
        self.end_label.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.end_label)
        self.main_widget.setLayout(self.main_layout)

        self.label_stylesheet = "QLabel {margin: 0px;font-size: 70pt;color:white}"
        self.end_label.setStyleSheet(self.label_stylesheet)

        # create order of sides and spots
        # shuffle sides first
        random.shuffle(self.sides)
        # create ordered list of tupples: side, spot
        my_sequence = []
        for el in self.sides:
            temp_seq = self.spots_dict[el]
            random.shuffle(temp_seq)
            for side in temp_seq:
                my_sequence.append((el,side))
        # print(my_sequence)

        # configure logging
        self.current_path = os.path.dirname(os.path.abspath(__file__))
        self.dump_path = os.path.join(self.current_path,LOG_FOLDER)
        try: 
            os.mkdir(self.dump_path)
        except:
            # print("Path exists")
            pass
        self.log_file_name = self.participant_id+self.session_id+"_"+datetime.now().strftime("%Y_%m_%d_%H_%M_%S")+".txt"
        self.log_path = os.path.join(self.dump_path,self.log_file_name)
        # log task settings
        f = open(self.log_path, "a")
        f.write("Participant ID: "+self.participant_id+"\n")
        f.write("Session ID: "+self.session_id+"\n")
        seq_str = "Current Sequence: "
        for el in my_sequence:
            side,spot = el
            seq_str = seq_str + side + spot + ","
        f.write(seq_str+"\n\n\n")
        f.close()

        # create a sequence with repetitions
        self.current_full_sequence = []
        for i in range(len(my_sequence)):
            for j in range(self.repetitionsNo):
                # add a tuple: side,spot,current repetition
                self.current_full_sequence.append((list(my_sequence[i])[0],list(my_sequence[i])[1],str(j+1)))
        # print(self.current_full_sequence)

        # send current side, spot, repetition
        self.scale_window = ScaleWidget(self.current_full_sequence[self.current_trial])
        self.scale_window.rating_received_sig.connect(self.rated)
        self.scale_window.showMaximized()

    def winEnumHandlerRTS( self,hwnd, ctx ):
        if win32gui.IsWindowVisible( hwnd ):
            print (hex(hwnd), win32gui.GetWindowText( hwnd ))
            if win32gui.GetWindowText( hwnd ) == "RTSm 105":
                win32gui.SetForegroundWindow(hwnd)

    def winEnumHandlerScale( self,hwnd, ctx ):
        if win32gui.IsWindowVisible( hwnd ):
            print (hex(hwnd), win32gui.GetWindowText( hwnd ))
            if win32gui.GetWindowText( hwnd ) == "Scale":
                win32gui.SetForegroundWindow(hwnd)

    @pyqtSlot(int) # when participant rates sensation, go to words
    def rated(self,rating):
        # log rating
        params = self.current_full_sequence[self.current_trial]
        f = open(self.log_path, "a")
        f.write("Side: "+params[0]+" Spot: "+params[1]+" No: "+params[2]+"\n")
        f.write("Rating: "+str(rating)+"\n")
        f.close()
        if self.scale_window != None:
            self.scale_window.close()
        self.words_window = WordsWidget(self.words)
        self.words_window.word_received_sig.connect(self.described)
        self.words_window.showMaximized()

    @pyqtSlot(str) # when participant describes sensation
    def described(self,word):
        category = "Unknown"
        if word in CATEGORY_DICT:
            category = CATEGORY_DICT[word]
        # log word
        f = open(self.log_path, "a")
        f.write("Description: "+word+"\n")
        f.write("Category: "+category+"\n\n")
        if self.current_trial + 1 == len(self.current_full_sequence):
            f.write("Task Completed")
        f.close()

        # put RTS software to focus to send pause signal (F1)
        win32gui.EnumWindows( self.winEnumHandlerRTS, None )
        time.sleep(self.sleep)
        keyboard.send("left alt")
        time.sleep(self.sleep)

        # # send 2 keystrokes F1
        # keyboard.send("F1")
        # time.sleep(self.sleep)
        # keyboard.send("F1")
        

        if self.words_window != None:
            time.sleep(self.sleep)
            self.words_window.close()
        if self.spot_counter +1 == self.repetitionsNo:
            self.spot_counter = 0
        else:
           self.spot_counter +=1 
        if self.current_trial + 1 == len(self.current_full_sequence):
            # end task
            self.showMaximized()
        else: # show next scale
            self.current_trial +=1
            # send current side, spot, repetition
            self.scale_window = ScaleWidget(self.current_full_sequence[self.current_trial])
            self.scale_window.rating_received_sig.connect(self.rated)
            self.scale_window.showMaximized()
            # bring the scale back to the front
            win32gui.EnumWindows( self.winEnumHandlerScale, None )
            

############################ end ManagerWidget

########################################
# SCALE CLASS
######################################

class ScaleWidget(QMainWindow):

    # signals
    rating_received_sig = pyqtSignal(int)

    def __init__(self,params):
        super(ScaleWidget, self).__init__()
        self.name = "Scale"
        self.app_width = 800
        self.app_height = 600
        self.setWindowTitle(self.name)
        # self.setWindowIcon(QtGui.QIcon(ICO))
        self.resize(self.app_width,self.app_height)
        # create info label text
        self.info_text = "Side: "+params[0]+" Spot: "+params[1]+" No: "+params[2]

        self.scale_widget = QWidget()
        self.setCentralWidget(self.scale_widget)
        self.main_layout = QVBoxLayout()
        self.main_layout.setContentsMargins(10,10,10,10)
        self.scale_widget.setLayout(self.main_layout)
        self.info_layout = QHBoxLayout()
        self.info_label = QLabel(self.info_text)
        self.info_layout.setAlignment(Qt.AlignRight | Qt.AlignTop)
        self.info_layout.addWidget(self.info_label)
        self.main_layout.addLayout(self.info_layout)

        self.slider_layout = QVBoxLayout()
        self.slider_layout.setContentsMargins(100,50,100,50)
        self.slider_widget = QSlider(Qt.Horizontal)
        self.slider_widget.setMinimum(0)
        self.slider_widget.setMaximum(100)
        self.slider_widget.setValue(50)
        self.slider_widget.setTickPosition(QSlider.NoTicks)
        self.slider_widget.setTickInterval(1)
        self.slider_widget.setSingleStep(1)
        QSS = """
            /* QSlider --------------------------------------  */
            QSlider {height: 100px;
                    color:white;}
            QSlider::handle:horizontal {
                background-color: blue;
                border: none;
            }
            QSlider::handle:horizontal:hover {
                background-color: blue;
            }
            QSlider::handle:horizontal:pressed {
                background-color: yellow;
            }
            """
        self.slider_widget.setStyleSheet(QSS)
        self.question_label = QLabel("How pleasant was that?\n")
        self.question_label.setAlignment(Qt.AlignCenter)
        self.slider_layout.addWidget(self.question_label)
        self.slider_layout.addWidget(self.slider_widget)
        self.main_layout.addLayout(self.slider_layout)

        self.scale_labels_layout = QHBoxLayout()
        self.scale_labels_layout.setContentsMargins(10,50,10,50)
        self.pleasant_label = QLabel("Very Pleasant")
        self.pleasant_label.setAlignment(Qt.AlignRight)
        self.unpleasant_label = QLabel("Very Unpleasant")
        self.unpleasant_label.setAlignment(Qt.AlignLeft)
        self.slider_labels_stylesheet = "QLabel {font-size: 40pt;color:white}"
        self.question_label.setStyleSheet(self.slider_labels_stylesheet)
        self.pleasant_label.setStyleSheet(self.slider_labels_stylesheet)
        self.unpleasant_label.setStyleSheet(self.slider_labels_stylesheet)
    
        self.scale_labels_layout.addWidget(self.unpleasant_label)
        self.scale_labels_layout.addWidget(self.pleasant_label)
        self.main_layout.addLayout(self.scale_labels_layout)


        # make big label
        self.label_stylesheet = "QLabel {margin: 0px;font-size: 20pt;color:white}"
        self.scale_widget.setStyleSheet(self.label_stylesheet)
        self.setStyleSheet("background-color: rgb(0,0,0);") 

        self.slider_widget.sliderReleased.connect(self.released)

    def released(self):
        rating = self.slider_widget.value()
        print('rated:',rating) 
        # emit signal that the brush was rated
        self.rating_received_sig.emit(rating)

############################ end ScaleWidget

########################################
# WORDS CLASS
######################################

class WordsWidget(QMainWindow):

    # signals
    word_received_sig = pyqtSignal(str)
    
    def __init__(self,words):
        super(WordsWidget, self).__init__()
        self.name = "Describe"
        self.app_width = 800
        self.app_height = 600
        self.setWindowTitle(self.name)
        # self.setWindowIcon(QtGui.QIcon(ICO))
        self.resize(self.app_width,self.app_height)
        # create info label text
        self.word_list = words

        # create list of coordinated for words: rows and col
        total_column_no = MAX_WORDS_IN_A_ROW
        coord = []
        col = 0
        row = 0
        for i in range(len(self.word_list)): 
            coord.append((row,col))
            if col+1 < total_column_no:
                col +=1
            else:
                col = 0
                row +=1

        self.main_widget = QWidget()
        self.setCentralWidget(self.main_widget)
        self.main_layout = QGridLayout()
        self.main_layout.setContentsMargins(10,10,10,10)
        self.main_layout.setAlignment(Qt.AlignCenter)
        self.main_widget.setLayout(self.main_layout)

        self.words_button_group = QButtonGroup()
        self.words_button_group.buttonClicked.connect(self.on_toggle)

        for i in range (len(self.word_list)):
            chbx = QCheckBox(self.word_list[i])
            # assign ids as in the list order to identify them later
            self.words_button_group.addButton(chbx,i)
            row,col = coord[i]
            self.main_layout.addWidget(chbx,row,col)


        # make big label
        # self.chbx_stylesheet = "QCheckBox {margin: 10px;padding: 15px; font-size: 15pt;color:white}"
        self.chbx_stylesheet = """
            /* QCheckBox --------------------------------------  */
            QCheckBox {margin: 10px;
                    padding:15px;
                    font-size: 15pt;
                    color:white
                    }
            QCheckBox::checked{
                color:yellow
            }
            
            """
        self.main_widget.setStyleSheet(self.chbx_stylesheet)
        self.setStyleSheet("background-color: rgb(0,0,0);") 

    def on_toggle(self):
        # read which was selected
        # print(self.words_button_group.checkedId())
        QApplication.processEvents()
        word = self.word_list[self.words_button_group.checkedId()]
        print("Word:", word)
        self.word_received_sig.emit(word)


############################ end WordsWidget

################################################################
#                                                              #
# EXECUTE GUI FROM MAIN                                        #
#                                                              #
################################################################
if __name__ == "__main__":
    # Always start by initializing Qt (only once per application)
    app = QApplication([])
    main_widget = MySettingsWidget()
    main_widget.show()
    app.exec_()
   

    print('Done')