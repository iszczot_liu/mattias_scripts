
import sys
import warnings
warnings.simplefilter("ignore", UserWarning)
sys.coinit_flags = 2
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import keyboard
import time
# import win32con
import win32gui
# import pywinauto
# from pywinauto.application import Application


class MySettingsWidget(QMainWindow):

    def __init__(self):
        super(MySettingsWidget, self).__init__()
        self.name = "Test"
        self.app_width = 400
        self.app_height = 80
        self.setWindowTitle(self.name)
        # self.setWindowIcon(QtGui.QIcon(ICO))
        self.resize(self.app_width,self.app_height)



        self.main_widget = QWidget()
        self.main_layout = QFormLayout()
        self.setCentralWidget(self.main_widget)       
        self.main_widget.setLayout(self.main_layout)
 

        self.start_btn = QPushButton("F1")
        self.main_layout.addRow("",self.start_btn)

        self.start_btn.clicked.connect(self.press_key)
       
    def winEnumHandler( self,hwnd, ctx ):
        if win32gui.IsWindowVisible( hwnd ):
            print (hex(hwnd), win32gui.GetWindowText( hwnd ))
            if win32gui.GetWindowText( hwnd ) == "RTSm 105":
                win32gui.SetForegroundWindow(hwnd)

    def winEnumHandler2( self,hwnd, ctx ):
        if win32gui.IsWindowVisible( hwnd ):
            print (hex(hwnd), win32gui.GetWindowText( hwnd ))
            if win32gui.GetWindowText( hwnd ) == "Test":
                win32gui.SetForegroundWindow(hwnd)

    # define keypress events
    def keyPressEvent(self,event):
        # if enter is pressed start button clicked
        print(event.key())

    def press_key(self):
        # co_app = Application(backend='uia').connect(title_re="^RTSm",found_index=0, timeout=5)
        # window = co_app.window(best_match='Dialog', found_index=0)
        # window.wait('ready')
        # # window.print_control_identifiers()
        # window.set_focus()
        # time.sleep(4)
        win32gui.EnumWindows( self.winEnumHandler, None )
        keyboard.send("F1")
        time.sleep(2)
        print("Find Task")
        # co_app1 = Application(backend='uia').connect(title_re="^Test",found_index=0, timeout=5)
        # print("Found task")
        # window1 = co_app1.window(best_match='Dialog', found_index=0)
        # # window1.print_control_identifiers()
        # window1.wait('ready')
        # window1.draw_outline()
        # time.sleep(2)
        # window1.minimize()
        # window1.restore()
        win32gui.EnumWindows( self.winEnumHandler2, None )
        print("Found and focused task")

########################## end MySettingsWidget


if __name__ == "__main__":
    # Always start by initializing Qt (only once per application)
    app = QApplication([])
    main_widget = MySettingsWidget()
    main_widget.show()
    app.exec_()
   

    print('Done')