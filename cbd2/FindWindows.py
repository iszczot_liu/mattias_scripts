import win32gui

def winEnumHandler( hwnd, ctx ):
    if win32gui.IsWindowVisible( hwnd ):
        print (hex(hwnd), win32gui.GetWindowText( hwnd ))
        if win32gui.GetWindowText( hwnd ) == "RTSm 105":
            win32gui.SetForegroundWindow(hwnd)

win32gui.EnumWindows( winEnumHandler, None )