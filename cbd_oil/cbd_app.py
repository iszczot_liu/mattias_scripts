# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 14:57:33 2020

@author: ilosz01
"""
from PyQt5 import QtGui 
from PyQt5.QtWidgets import QWidget, QMainWindow,QTableWidget,QTableWidgetItem,QButtonGroup
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph.dockarea import *
import sys
import os
import collections
import random
import time
from time import strftime
from datetime import datetime
import csv


# distance to brush in cm
DISTANCE_TO_BRUSH = 9.0
UNIQUE_SPEEDS = [0.3,1,3,10,30]
# 0 = LEFT ARM; 1 = RIGHT ARM
UNIQUE_ARMS = [0,1]
# how many times to run each arm+speed combination
UNIQUE_COMBINATIONS_NO = 20


# define stylesheet
STYLESHEET = \
            ".QLineEdit,.QLabel,.QPushButton,.QTabWidget, .QComboBox {\n" \
            + "padding: 7px;\n" \
            + "margin-left:10px;\n" \
            + "margin-right:10px;\n" \
            + "margin-top:10px;\n" \
            + "margin-bottom:10px;\n" \
            + "font-size: large;\n" \
            + "font-weight: bold;\n" \
            + "}" 

################################################################
# DEFINE A TOP LEVEL WIDGET TO HOLD EVERYTHING                 #
################################################################
class MyMainWidget(QMainWindow):

    # emit signal to close all other windows
    app_closed = pyqtSignal()
    allow_next_sequence = pyqtSignal()
    allow_rating = pyqtSignal()
    def __init__(self):
        super(MyMainWidget, self).__init__()
        self.name = "CBD"
        self.app_width = 800
        self.app_height = 400
        self.top_buttons_height = 50
        self.setWindowTitle(self.name)
        self.resize(self.app_width,self.app_height)
        self.participant_id = 0
        self.path2save = None
        self.speeds = UNIQUE_SPEEDS
        self.arms_no = UNIQUE_ARMS
        #generate all combinations of arm+speed
        self.all_arm_speed_combinations = []
        for i in range(len(self.arms_no)):
            for j in range(len(self.speeds)):
                arm_speed_tuple = (self.arms_no[i],self.speeds[j])
                self.all_arm_speed_combinations.append(arm_speed_tuple)
        # print("all_arm_speed_combinations:",self.all_arm_speed_combinations)
        # Each combination is run block_repetitions(20) times for a total of 200 trials ((5 speeds x 2 arms) x 20 trials)
        self.block_repetitions = UNIQUE_COMBINATIONS_NO
        self.block_repetitions_counter = 0
        self.all_combinations = []
        self.all_ratings = []
        # window a subject to see the scale or empty screen
        self.participant_window = None
        self.researchers_preview = None
        
        # Create button widgets to be placed inside top dock widget.
        self.counter_label = QtGui.QLabel("Block No:")
        self.counter_number = QtGui.QLabel(str(self.block_repetitions_counter))
        self.participant_id_label = QtGui.QLabel("Participant ID:")
        self.participant_id_text = QtGui.QLineEdit(str(self.participant_id))
        self.participant_id_text.setValidator(QtGui.QIntValidator())
        self.start_block_btn = QtGui.QPushButton('Start Block')


        # use docks from pyqtgraph
        self.area = DockArea()
        self.setCentralWidget(self.area)
        self.top_dock_widget = Dock("Dock1", size=(self.app_width, self.top_buttons_height))
        self.top_dock_widget.hideTitleBar()
        self.area.addDock(self.top_dock_widget,'left')
        self.bottom_dock_widget = Dock("Dock2", size=(self.app_width,self.app_height-self.top_buttons_height))
        self.bottom_dock_widget.hideTitleBar()
        self.area.addDock(self.bottom_dock_widget, 'bottom', self.top_dock_widget)  # place the bottom dock at bottom edge of top dock

        # Create a layout to manage the buttons size and position
        self.box_layout = QtGui.QHBoxLayout()
        # set the spacing around the layout
        self.box_layout.setContentsMargins(10,10,10,10)
        # place widgets to the layout in their proper positions
        self.box_layout.addWidget(self.counter_label)
        self.box_layout.addWidget(self.counter_number)
        self.box_layout.addWidget(self.participant_id_label)
        self.box_layout.addWidget(self.participant_id_text)
        self.box_layout.addWidget(self.start_block_btn)
        # create a widget for dock from pyqtgraph
        self.top_widget = QWidget()
        # add that widget to the dock
        self.top_dock_widget.addWidget(self.top_widget)
        self.top_widget.setLayout(self.box_layout)

        # use stylesheet
        self.area.setStyleSheet(STYLESHEET)

        # connect actions to buttons
        self.start_block_btn.clicked.connect(self.start_block)       
        ##################################################################
        # end init

#################################################################
# Define what buttons do    
    # define what clicking on start block does
    def start_block(self):
        self.prompt_to_enter_path()
        self.counter_number.setText(str(self.block_repetitions_counter+1))
        QtGui.QApplication.processEvents() 
        if self.block_repetitions_counter < self.block_repetitions:
            # increment block repetitions counter 
            self.block_repetitions_counter +=1
            # shuffle combinations
            self.all_arm_speed_combinations = random.sample(self.all_arm_speed_combinations, len(self.all_arm_speed_combinations))
            # create window for participant
            self.participant_window = ParticipantWindow(self,self.all_arm_speed_combinations)
            # read participant id from text field
            self.participant_id = int(self.participant_id_text.text())
            # print('participant id',self.participant_id)
            # disable buttons
            self.disable_buttons()
            # enable back all buttons when popup is closed
            self.participant_window.trigger.connect(self.enable_buttons)
            # receive rated signal when participant rates the feeling
            self.participant_window.rated.connect(self.next_sequence)
            self.participant_window.show()
            
            # meanwhile display lower part of the researchers block
            # check if previous is still visible
            if self.researchers_preview != None:
                    self.researchers_preview.close()
                    # set research area back to None
                    self.researchers_preview = None
                
            if self.researchers_preview is None:
                self.researchers_preview = ResearcherBlockDisplay(self,self.app_height-self.top_buttons_height,self.app_width,self.all_arm_speed_combinations)
                self.researchers_preview.brushing_finished.connect(self.rate)
                # add that widget to the dock
                self.bottom_dock_widget.addWidget(self.researchers_preview)
            # self.participant_window.show_scale()
        else :
            print('All required repetitions performed')
################################################################################################################
            
    # function to disable buttons, all buttons but stop button
    def disable_buttons(self):
        self.start_block_btn.setEnabled(False)
        self.participant_id_text.setReadOnly(True)
        

    # function to enable buttons, all buttons but stop button
    def enable_buttons(self):
        self.start_block_btn.setEnabled(True)

    # ask for folder to save data
    def prompt_to_enter_path(self):
        if self.block_repetitions_counter == 0:
            while self.path2save == None or len(self.path2save) == 0:
                self.path2save = QtGui.QFileDialog.getExistingDirectory(self,"Choose Directory")
            print(self.path2save)

    # how to save data
    def save_session(self):
        if len(self.all_combinations) > 1:
            out_file = str(self.participant_id)+'_' + strftime("%m%d%y_%H%M") + '.csv'
            print('saving file in',self.path2save,out_file)
            # dump in the end
            with open(os.path.join(self.path2save,out_file), 'w', newline='') as csvfile: 
                dump_writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        # excel did not recognize comas as separators, I had to add this line on top od csv file
                        # https://kb.paessler.com/en/topic/2293-i-have-trouble-opening-csv-files-with-microsoft-excel-is-there-a-quick-way-to-fix-this
                dump_writer.writerow(["sep=,"])
                first_row = ['Date',strftime("%m/%d/%y")]
                dump_writer.writerow(first_row)
                second_row = ['Participant ID:',self.participant_id]
                dump_writer.writerow(second_row)
                third_row = ['Expected Repeat No:',self.block_repetitions]
                dump_writer.writerow(third_row)
                forth_row = ['Actual Repetitions No:',self.block_repetitions_counter]
                dump_writer.writerow(forth_row)
                fifth_row = ['All speads (cm_per_sec)',self.speeds]
                dump_writer.writerow(fifth_row)
                sixth_row = ['All arms',self.arms_no]
                dump_writer.writerow(sixth_row)
                break_row = ['']
                dump_writer.writerow(break_row)
                # write headers
                headers = ['Trial','Arm(0=left,1=right)','Speed','Rating']
                dump_writer.writerow(headers)
                # create trial counters
                trial_no = [i+1 for i in range(len(self.all_combinations))]
                for i in range(len(self.all_combinations)):
                    # create blank row every tenth
                    if i%(len(self.all_arm_speed_combinations)) == 0 and i!=0:
                        dump_writer.writerow([''])
                    my_row = [trial_no[i]]
                    arm,speed = self.all_combinations[i][0], self.all_combinations[i][1]
                    my_row.append(arm)
                    my_row.append(speed)
                    rating = self.all_ratings[i] if i < len(self.all_ratings) else ''
                    my_row.append(rating)
                    dump_writer.writerow(my_row)



    @pyqtSlot(int) # when participant rates feeling, allow to go to next sequence
    def next_sequence(self,rating):
        # print('received rating',rating)
        self.all_ratings.append(rating)
        self.allow_next_sequence.emit()


    @pyqtSlot(list) # when brushing has finished allow to rate
    def rate(self,current_combination):
        # print('brushing finished received',current_combination)
        self.all_combinations.append(current_combination)
        self.allow_rating.emit()


    # emit signal when app is closed
    def closeEvent(self, event):  
        # save data to file
        self.save_session()
        self.app_closed.emit() 
################################################################
# end MyMainWidget class

################################################################
# CLASS WITH PARTICIPANT WIDGET                                #
################################################################
class ParticipantWindow(QMainWindow):
    # pyqt Signal has to be defined on class level
    # and not in init method !!
    trigger = pyqtSignal()
    rated = pyqtSignal(int)

    def __init__(self,parent_window,all_arm_speed_combinations):
        super(ParticipantWindow, self).__init__()
        self.parent_window = parent_window
        self.parent_window.allow_rating.connect(self.rate_feeling)
        self.parent_window.app_closed.connect(self.exit_app)
        # self.parent_window.app_closed.connect(self.exit_app)
        self.resize(800,300)
        self.setWindowTitle("CBDapp")

        # how many times to perform brushing to make sure all combinations of arm and speed were completed
        self.in_block_repetitions = len(all_arm_speed_combinations)
        self.in_block_repetitions_counter = 0
        # a list to hold all ratings for the current block
        self.all_block_ratings = []
        # use docks from pyqtgraph
        self.area = DockArea()
        self.setCentralWidget(self.area)
        self.dock_widget = Dock("Dock1", size=(1, 1))
        self.dock_widget.hideTitleBar()
        self.dock_widget.setContentsMargins(10,10,10,10)
        self.area.addDock(self.dock_widget,'left')
        # create displayed widget
        self.participant_widget = QWidget()
        self.participant_layout = QtGui.QHBoxLayout()
        self.participant_label = QtGui.QLabel("Relax")
        self.participant_label.setAlignment(QtCore.Qt.AlignCenter)
        participant_stylesheet = "QLabel {margin: 30px;font-size: 50pt}"
        self.participant_widget.setStyleSheet(participant_stylesheet)
        # add layout to widget
        self.participant_widget.setLayout(self.participant_layout)
        # add widget to the dock
        self.dock_widget.addWidget(self.participant_widget)
        self.participant_layout.addWidget(self.participant_label)
        self.slider_widget = None
        # use stylesheet
        self.area.setStyleSheet(STYLESHEET)


    # displays rating scale to the participant    
    def show_scale(self):
        print('in_block_repetitions_counter participant:',self.in_block_repetitions_counter+1)
        # show scale only when inner loop is not over
        if self.in_block_repetitions_counter < self.in_block_repetitions:
            # clear relax text
            self.participant_label.setText("")
            self.slider_widget = QtGui.QSlider(Qt.Horizontal)
            self.slider_widget.setMinimum(0)
            self.slider_widget.setMaximum(100)
            self.slider_widget.setValue(50)
            self.slider_widget.setTickPosition(QtGui.QSlider.TicksBelow)
            self.slider_widget.setTickInterval(1)
            self.slider_widget.setSingleStep(1)
            self.participant_layout.addWidget(self.slider_widget)
            # self.slider_widget.valueChanged.connect(self.valuechange)
            self.slider_widget.sliderReleased.connect(self.released)
        
      
    def released(self):
        rating = self.slider_widget.value()
        # print('rated:',rating)
        # add rating to the block rating list
        self.all_block_ratings.append(rating)
        self.slider_widget.close()  
        # show relax again
        self.participant_label.setText("Relax")
        # increment counter after each rating
        self.in_block_repetitions_counter +=1
        # emit signal that the brush was rated
        self.rated.emit(rating)
      

    
    # emit signal when widget is closed, so that the main widget can detect when window is closed
    def closeEvent(self, event):  
        self.trigger.emit()  
        
    @pyqtSlot() # destroy window if parent closed
    def exit_app(self):
        self.close()
        del(self)

    @pyqtSlot() 
    # after brushing ended, allow to show rating scale
    def rate_feeling(self):
        # print('allow rating received')
        self.show_scale()
################################################################
# CLASS WITH RESEARCHER WIDGET                                 #
################################################################             
class ResearcherBlockDisplay(pg.LayoutWidget):
    # pyqt Signal has to be defined on class level
    # and not in init method !!
    brushing_finished = pyqtSignal(list)

    def __init__(self,parent, height,width,all_arm_speed_combinations):
        print('Current random shuffle:',all_arm_speed_combinations)
        super(ResearcherBlockDisplay, self).__init__()
        pg.setConfigOption('background', 'w')
        self.resize(width,height)
        self.parent = parent
        self.parent.allow_next_sequence.connect(self.update_screen)
        self.all_arm_speed_combinations = all_arm_speed_combinations
        # how many times to perform brushing to make sure all combinations of arm and speed were completed
        self.in_block_repetitions = len(self.all_arm_speed_combinations)
        self.in_block_repetitions_counter = 0
        self.allow_progress_bar = True
        self.allow_progress_start = False
        # all_arm_speed_combinations are already shuffled when block is started
        # show params (in the shuffled order, same iterator as in block counter)
        arm,speed = self.all_arm_speed_combinations[self.in_block_repetitions_counter]
        # calculate time for progress bar
        # distance * 1 sec / speed
        self.my_time = DISTANCE_TO_BRUSH/speed
        print('speed:',speed,'expected time:',self.my_time)
        # which arm
        arm_str = '--> Left Arm' if arm == 0 else '<-- Right Arm'
        show_text = arm_str + '; Speed: '+ str(speed)+'\n\nPress Space Bar to show speed'
        self.current_combination_list = [arm,speed]
        # define params field
        self.params = QtGui.QLabel(show_text)
        self.params.setAlignment(QtCore.Qt.AlignCenter) 
        self.addWidget(self.params,row=0,col=0)
        self.setFocusPolicy(Qt.StrongFocus)
        self.researcher_stylesheet = "QLabel {margin: 30px;font-size: 28pt}"
        self.params.setStyleSheet(self.researcher_stylesheet)
        
    def keyPressEvent(self,event):
        if event.key() == QtCore.Qt.Key_Space and self.allow_progress_bar == True:
            self.speed_progress_bar = QtGui.QProgressBar(self)
            self.speed_progress_bar.setAlignment(QtCore.Qt.AlignCenter) 
            arm = self.current_combination_list[0]
            speed = self.current_combination_list[1]
            arm_str = '--> Left Arm' if arm == 0 else '<-- Right Arm'
            show_text = arm_str + '; Speed: '+ str(speed)
            self.params.setText(show_text)
            self.addWidget(self.speed_progress_bar,row=1,col=0)
            self.hint_text = QtGui.QLabel('Press B to begin')
            self.hint_text.setAlignment(QtCore.Qt.AlignCenter) 
            self.hint_text.setStyleSheet(self.researcher_stylesheet)
            self.addWidget(self.hint_text,row=2,col=0)
            self.allow_progress_bar = False
            self.allow_progress_start = True
        if event.key() == QtCore.Qt.Key_B and self.allow_progress_start == True:
            self.allow_progress_start = False
            for i in reversed(range(4)):
                self.hint_text.setText(str(i))
                QtGui.QApplication.processEvents() 
                time.sleep(1)
            # start speed bar
            # setting for loop to set value of progress bar 
            start = datetime.now()
            for i in range(1,100): 
                # slowing down the loop 
                self.speed_progress_bar.setValue((i+1))
                QtGui.QApplication.processEvents() 
                # wait to achieve desired time
                time.sleep(self.my_time*0.01)
            print('Actual speed:',(datetime.now()-start).seconds, 'sec')
            # increment in block counter
            self.in_block_repetitions_counter +=1
            # emit signal that it is time to rate
            self.brushing_finished.emit(self.current_combination_list)

        super(ResearcherBlockDisplay, self).keyPressEvent(event)
    
    @pyqtSlot() # perform after participant rated feeling
    def update_screen(self):
        self.raise_()
        self.activateWindow()
        if self.in_block_repetitions_counter < self.in_block_repetitions:
            print('')
            print('next inner block:',self.in_block_repetitions_counter+1)
            # take next arm+speed combination from the list
            # all_arm_speed_combinations are already shuffled when block is started
            # show params (in the shuffled order, same iterator as in block counter)
            arm,speed = self.all_arm_speed_combinations[self.in_block_repetitions_counter]
            # calculate time for progress bar
            # distance * 1 sec / speed
            self.my_time = DISTANCE_TO_BRUSH/speed
            print('speed:',speed,'expected time:',self.my_time)
            # which arm
            arm_str = '--> Left Arm' if arm == 0 else '<-- Right Arm'
            show_text = arm_str + '; Speed: '+ str(speed)+'\n\nPress Space Bar to show speed'
            self.params.setText(show_text)
            self.current_combination_list = [arm,speed]
            # clear progress bar and hint
            self.speed_progress_bar.close()
            self.hint_text.setText('')
            self.allow_progress_bar = True
        else:
            self.params.setText('End of the Block.\n\nTime for a quick break...')
            # clear progress bar and hint
            self.speed_progress_bar.close()
            self.hint_text.setText('')


    def set_params(self,string):
        self.params.setText(string)
################################################################
#                                                              #
# EXECUTE GUI FROM MAIN                                        #
#                                                              #
################################################################
if __name__ == "__main__":
    # Always start by initializing Qt (only once per application)
    app = QtGui.QApplication([])
    main_widget = MyMainWidget()
    main_widget.show()
    app.exec_()
   

    print('Done')

