
*** REQUIREMENTS ***

Anaconda3 (Anaconda3-2019.10-Windows-x86_64)
PyQtGraph
pip install pyqtgraph
or
conda install -c conda-forge pyqtgraph

________________

| DESCRIPTION  |
________________

Variables to set:
-	Participant ID
-	Speeds (default: 0.3, 1, 3, 10, 30 cm/s)
-	No.  arms (default: 2)
-	No. repeats (default: 20? – less if breaking it into multiple testing sessions to give a rest)

Each trial:
-	Tell experimenter which speed and arm to apply the brush stimulus
-	Provide timing cues (visual cue moving + optionally audio) to apply on Experimenter screen
-	Display VAS on Participant screen
-	Wait for participant response & record it
-	Each combination is run 20 times for a total of 200 trials ((5 speeds x 2 arms) x 20 trials)
-	Pause for every block of 10 unique combinations.
Experiment loop:
-	Each combination of speed and arms x repeats, randomly ordered
-	Save data with stimulus description and VAS rating for each trial
