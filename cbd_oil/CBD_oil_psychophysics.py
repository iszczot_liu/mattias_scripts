import os
import csv
import random

participant_id= 0
speeds = [0.3,1,3,10,30]
arms_no = [0,1]
repeat_no = 10

path2write = "C:\\Users\\ilosz01\\OneDrive - Linköpings universitet\\Mattias\\Dump"
out_file = "out.csv"

# randomly pick an arm + speed combination
# repeat the random picks repeat_no times
# for i in range(repeat_no):
#     rand_arm = random.choice(arms_no)
#     rand_speed = random.choice(speeds)
#     current_arm_speed_combination = [rand_arm,rand_speed]
    # print('current_arm_speed_combination',current_arm_speed_combination)

# or
# all possible combinations
all_arm_speed_combinations = []
for i in range(len(arms_no)):
    for j in range(len(speeds)):
        arm_speed_tuple = (arms_no[i],speeds[j])
        all_arm_speed_combinations.append(arm_speed_tuple)
print(all_arm_speed_combinations)
# now randomly shuffle those repeat_no times
for i in range(repeat_no):
    new_sequence = random.sample(all_arm_speed_combinations, len(all_arm_speed_combinations))
    print(new_sequence)

# dump in the end
with open(os.path.join(path2write,out_file), 'w', newline='') as csvfile: 
    dump_writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            # excel did not recognize comas as separators, I had to add this line on top od csv file
            # https://kb.paessler.com/en/topic/2293-i-have-trouble-opening-csv-files-with-microsoft-excel-is-there-a-quick-way-to-fix-this
    dump_writer.writerow(["sep=,"])
    first_row = ['Participant ID:',participant_id]
    dump_writer.writerow(first_row)
    second_row = ['Repeat No:',repeat_no]
    dump_writer.writerow(second_row)